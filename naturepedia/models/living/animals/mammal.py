from django.db import models

from naturepedia.models.living.animals.animal import Animal


class Mammal(Animal):
    nb_legs = 4
