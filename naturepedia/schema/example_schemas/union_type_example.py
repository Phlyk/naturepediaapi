import graphene


class CreateNumberTooLargeError(graphene.ObjectType):
	message = graphene.String(required=True)

class CreateNumberTooSmallError(graphene.ObjectType):
	message = graphene.String(required=True)

class CreateNumberSuccess(graphene.ObjectType):
	number = graphene.Int(required=True)

class CreateNumberPayload(graphene.Union):
	class Meta:
		types = (
			CreateNumberSuccess,
			CreateNumberTooSmallError,
			CreateNumberTooLargeError,
		)

class CreateNumberMutation(graphene.Mutation):
    class Arguments:
        number = graphene.Int(required=True)

	Output = CreateNumberPayload

	def mutate(self, info, number):
		if number > 10:
			return CreateNumberTooLargeError(message="Number is too large")
		if number < 5:
			return CreateNumberTooSmallError(message="Number is too small")
		return CreateNumberSuccess(number=number)
