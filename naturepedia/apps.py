from django.apps import AppConfig


class NaturepediaConfig(AppConfig):
    name = 'naturepedia'
