import logging
import time
from abc import ABC, abstractmethod

import graphene
from django.db.models import Q
from graphene import ID, Boolean, Field, Int, List, Mutation, ObjectType, String, relay
from graphene.relay import Node
from graphene_django.filter.fields import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType
from graphql.error import GraphQLError

from naturepedia.base.enum.colour_enum import Colour
from naturepedia.helper.dict_helper import set_if_not_none
from naturepedia.models.living.animals.fish import Fish
from naturepedia.models.living.living_traits.colour_entity import ColourEntity
from naturepedia.models.living.living_traits.size_entity import SizeEntity
from naturepedia.schema.living.animals.animal_input_type import AnimalInput, DeleteAnimalInput, InputQFilterBuilder

fish_logger = logging.getLogger(__name__)


class SizeType(DjangoObjectType):
    class Meta:
        model = SizeEntity
        interfaces = (relay.Node, )
        filter_fields = []


class ColourType(DjangoObjectType):
    colour = graphene.Enum.from_enum(Colour)

    class Meta:
        model = ColourEntity
        interfaces = (relay.Node, )
        filter_fields = ["colour"]

    def resolve_colour(root, info):
        return Colour(root.colour.capitalize()).name  # unsure why DJANGO ORM has made the entries all caps...


class FishType(DjangoObjectType):
    evolved_in = String()
    # colour = String(description="the colour of the fish but not nested like in DB")

    class Meta:
        model = Fish
        interfaces = (relay.Node, )
        # fields = ['english_name'] #- to reduce fields available on the object_type
        filter_fields = {
            "english_name": ['exact', 'icontains', 'istartswith'],
            "colour__colour": ['istartswith'],
            "size__min_height": ["gt"]
        }

    # @classmethod - not sure what value this is adding
    # def get_node(cls, info, id):
    #     return Fish.objects.get(pk=id)

    def resolve_evolved_in(root, info, **kwargs):
        return f"{root.evolved_in} Million years ago"


class GiveMeThemGoodOlFishesBoy(ObjectType):
    fish_node = Node.Field(FishType)
    all_fish_connection = DjangoFilterConnectionField(FishType)

    all_fishies_list = List(FishType)
    fish_by_id = Field(FishType, global_id=ID(required=True))
    fish_by_consumes = Field(FishType, consumes=String(required=True))

    def resolve_all_fishies_list(root, info, **kwargs):
        return Fish.objects.all()

    def resolve_fish_by_id(root, info, global_id):
        fish = Node.get_node_from_global_id(info, global_id)  # will return the actual fish entity
        fish_type, fish_id = Node.from_global_id(global_id)  # will base64 decode into a tuple
        print(fish)
        print(fish_type)
        print(fish_id)

        return Fish.objects.get(pk=global_id)

    def resolve_fish_by_consumes(root, info, consumes):
        return Fish.objects.get(consumes=consumes)


class FishInput(AnimalInput):
    nb_fins = Int(required=False, default_value=3, description="how many fins yer wee fishy have?")


class DeleteFishInput(DeleteAnimalInput):
    nb_fins = Int(description="how many fins yer wee fishy have?")


class RelayAddFish(relay.ClientIDMutation):
    fish = Field(FishType)
    ok = Boolean()
    errors = List(String, required=True)
    class Input:
        fish_input = FishInput(required=True, description="the fields required to create a fish")
        random = String(description="just testing")

    @classmethod
    def mutate_and_get_payload(cls, root, info, fish_input, **input):
        errors = []
        try:
            new_fish = Fish.create(**fish_input)
            new_fish.save()
            ok = True
        except Exception as e:
            fish_logger.error(f"crap: {str(e)}")
            errors.append(str(e))
            ok = False
            new_fish=None
            # raise GraphQLError(f"crap: {str(e)}")
        return RelayAddFish(fish=new_fish, ok=ok, errors=errors)

class AddFish(Mutation):
    fish = Field(FishType)
    # ok = Boolean()
    # errors = List(String, required=True, )

    class Input():
        fish_input = FishInput(required=True, description="the fields required to create a fish")

    def mutate(parent, info, fish_input: FishInput):
        time.sleep(10)
        try:
            new_fish = Fish.create(**fish_input)  # could also define an object manager create method (Fish.objects.create())
            new_fish.save()
        except Exception as e:
            fish_logger.error("crap: " + str(e))
            raise GraphQLError("Dat fish won't get created foo")
        return AddFish(fish=new_fish)


class UpdateFishById(Mutation):
    fish_updated = Field(FishType)

    class Input():
        fish_id = ID(required=True, description="the ID of yer wee fishy to change for all eternity")
        fish_input = FishInput(required=True, description="tell us the fishy ye get wrong the first time")

    def mutate(root, info, fish_id, fish_input):
        time.sleep(10)
        fish_input['colour'] = ColourEntity.objects.get(colour=fish_input['colour'].colour)  # would do same with sizentity, also ugly but time...
        Fish.objects.filter(pk=fish_id).update(**fish_input)
        # got_fish = Fish.objects.get(pk=fish_id)
        # for (key, value) in got_fish.__dict__.items():
        #     setattr(got_fish, key, value)
        # got_fish.save()
        updated_fish = Fish.objects.get(pk=fish_id)  # have to do this because django orm does SQL update directly (could fetch the entity too)
        return UpdateFishById(fish_updated=updated_fish)


class BulkUpdateFish(Mutation):
    nb_fish_updated = Int()
    # fish_updated = List(FishType)

    class Input():
        fish_search = FishInput(required=True, description="Yer searching fields")
        fish_fields_to_update = FishInput(required=True, description="What ye want changed in ye fish")

    def mutate(root, info, fish_search, fish_fields_to_update):
        input_q_filter = InputQFilterBuilder(Q.AND).build_filter_from_input(fish_search)
        nb_fish_updated = Fish.objects.filter(fish_search).update(**fish_fields_to_update)  # bulk_update
        return BulkUpdateFish(nb_fish_updated=nb_fish_updated)


class DeleteFish(Mutation):
    nb_deleted_fish = Int()
    filter_builder: InputQFilterBuilder

    class Input():
        delete_fish_input = DeleteFishInput(required=True, description="tell us the fishy fthat shouldn't exist")

    def mutate(root, info, delete_fish_input):
        input_q_filter = DeleteFish.filter_builder.build_filter_from_input(delete_fish_input)
        nb_deleted, deleted_fish_count = Fish.objects.filter(input_q_filter).delete()
        if not nb_deleted:
            raise GraphQLError("No fish were found to be deleted! Be more precise")  # change this to an errors return field
        return DeleteFish(nb_deleted_fish=nb_deleted)
        # will return the deleted fish when soft delete is enabled using `django-softdelete`
        # or DIY - https://adriennedomingus.com/blog/soft-deletion-in-django

    @staticmethod
    @abstractmethod
    def create_filter_builder() -> InputQFilterBuilder:
        pass


class DeleteFishInclusive(DeleteFish):
    @staticmethod
    def create_filter_builder():
        return InputQFilterBuilder(filter_method=Q.AND)

    @staticmethod
    def mutate(root, info, delete_fish_input):
        inclusive_filter_builder = DeleteFishInclusive.create_filter_builder()
        DeleteFish.filter_builder = inclusive_filter_builder
        return DeleteFish.mutate(root, info, delete_fish_input)


class DeleteFishExclusive(DeleteFish):
    @staticmethod
    def create_filter_builder():
        return InputQFilterBuilder(filter_method=Q.OR)

    @staticmethod
    def mutate(root, info, delete_fish_input):
        exclusive_filter_builder = DeleteFishExclusive.create_filter_builder()
        DeleteFish.filter_builder = exclusive_filter_builder
        return DeleteFish.mutate(root, info, delete_fish_input)


class DeleteFishById(Mutation):
    nb_deleted_fish = Int()

    class Input():
        fish_id = ID(required=True, description="The ID of the fish to boom")

    def mutate(root, info, fish_id):
        nb_deleted, deleted_fish_count = Fish.objects.get(pk=fish_id).delete()
        if not nb_deleted:
            raise GraphQLError("No fish were found to be deleted! Be more precise")
        return DeleteFish(nb_deleted_fish=nb_deleted_fish)


class DemFishesArentGoodEnoughForMe(ObjectType):
    relay_add_fish = RelayAddFish.Field(description="add a fish using relay")
    add_fish = AddFish.Field(description="the endpoint to add a fish")
    update_fish_by_id = UpdateFishById.Field(description="That fish need updatin'")
    bulk_update_fish = BulkUpdateFish.Field(description="When Ye need to update more than one fishy type")
    delete_fish_inclusive_search = DeleteFishInclusive.Field(description="Delete fish where the fish must match a combination of a fields")
    delete_fish_exclusive_search = DeleteFishExclusive.Field(description="Delete fish where every fish matching a field is deleted")
    delete_fish_by_id = DeleteFishById.Field(description="Pass the ID of the fish to go boom & it will")
