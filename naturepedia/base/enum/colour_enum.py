from enum import Enum


class Colour(Enum):
    NONE = 'No-colour'
    RED = 'Red'
    BLUE = 'Blue'
    YELLOW = 'Yellow'
    PURP = 'Purpz'
    GREEN = 'Green'
    BLACK = 'Black'
    WHITE = 'White'
    GREY = 'Grey'

    @classmethod
    def choices(cls):
        return tuple((colour.name, colour.value) for colour in cls)

    @classmethod
    def keys(cls):
        return [colour.name for colour in cls]

    @classmethod
    def values(cls):
        return [colour.value for colour in cls]
