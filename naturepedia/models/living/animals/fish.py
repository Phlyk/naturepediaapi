from django.db import models

from naturepedia.models.living.animals.animal import Animal
from naturepedia.schema.living.animals.animal_input_type import ColourInput, SizeInput


class Fish(Animal):
    nb_fins = models.PositiveSmallIntegerField("Number of fins the fish has")

    @classmethod
    def create(
        cls,
        english_name: str,
        evolved_in: int,
        colour: ColourInput,
        nb_fins: int,
        size_dict: SizeInput,
        consumes: str,
        scientific_name: str = None
    ):
        # the scientific name is defaulted to none because in the InputObjectType a value of none excludes it from the payload
        fish = super().create(
            english_name=english_name,
            consumes=consumes,
            scientific_name=scientific_name,
            evolved_in=evolved_in,
            colour=colour,
            size_dict=size_dict
        )
        fish.nb_fins = nb_fins
        return fish

    # Can do some cool aggregations using annotate & count to see which fish is the boss
