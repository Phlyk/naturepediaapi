import json

import graphene

from naturepedia.helper.random_string import makeRandomStringLikeAMofugga


class SquibildyPuff(graphene.ObjectType):
    id = graphene.ID()
    name = graphene.String()
    randomtext = graphene.String()
    full_name = graphene.String()

    def resolve_full_name(root, info, **kwargs):
        return f"{root.name}+-+{root.randomtext}"

    def __str__(self):
        return self.name

class GiveMeASquibildyPuff(graphene.ObjectType):
    my_squibildy_puff = graphene.Field(
        SquibildyPuff,
        squib_name=graphene.String(default_value="squibildy puffs"),
        base_text=graphene.String(),
        size_of_text=graphene.Int(default_value=40)
    )

    @staticmethod
    def resolve_my_squibildy_puff(root, info, squib_name, base_text, size_of_text):
        random_af = makeRandomStringLikeAMofugga(base_text, size_of_text)
        randomIsh = SquibildyPuff(id=57, name=squib_name, randomtext=random_af)
        return randomIsh

class MakeSquibildyPuff(graphene.Mutation):
    squibildyPuff = graphene.Field(SquibildyPuff)

    class Arguments:
        name = graphene.String(required=True)
        randomtextbase = graphene.String()

    def mutate(root, info, name, randomtextbase="OMG_DEFAULT"):
        random_text = makeRandomStringLikeAMofugga(randomtextbase)
        pappy_chaps = SquibildyPuff(
            name=name,
            randomtext=random_text
        )
        return MakeSquibildyPuff(squibildyPuff=pappy_chaps)

class MakeMeASquibildyPuff(graphene.ObjectType):
    make_me_a_squibildy_puff = MakeSquibildyPuff.Field()

# Stoopid test queries
# """
# query flerpysqwips {
#   mySquibildyPuff(baseText: "yerpyderp", sizeOfText: 150) {
#     id,
#     name
#     fullName,
#     __typename
#   }
# }

# mutation omg {
#   makeMeASquibildyPuff(name: "iluvmysqwibz") {
#     squibildyPuff {
#       id
#       name
#       fullName
#     }
#   }
# """
