from .bird import Bird
from .fish import Fish
from .insect import Insect
from .mammal import Mammal
