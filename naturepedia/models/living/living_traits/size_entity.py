from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver

from naturepedia.models.naturepedia_model import NaturepediaModel


class SizeEntity(NaturepediaModel):
    min_height = models.PositiveIntegerField("Minimum height recorded", help_text="in mm", default=1)
    max_height = models.PositiveIntegerField("Maximum height recorded", help_text="in mm", default=5)
    avg_height = models.PositiveIntegerField("Average height", help_text="in mm")
    min_weight = models.PositiveIntegerField("Minimum weight recorded", help_text="in grams", default=1)
    max_weight = models.PositiveIntegerField("Maximum weight recorded", help_text="in grams", default=5)
    avg_weight = models.PositiveIntegerField("Average weight", help_text="in grams")

    class Meta:
        db_table = 'naturepedia_size'

    def __str__(self):
        return f"avg_height:{self.avg_height},avg_weight:{self.avg_weight}"

    @classmethod
    def create_default(cls):
        return SizeEntity(
            min_height=1,
            max_height=5,
            avg_height=4,
            min_weight=1,
            max_weight=5,
            avg_weight=3
        )

@receiver(pre_save, sender=SizeEntity)
def pre_size_entity_save(sender, instance, **kwargs):
    instance.avg_height = int(instance.min_height + instance.max_height / 2)
    if instance.min_weight is not None and instance.max_weight is not None:
        instance.avg_weight = int(instance.min_weight + instance.max_weight / 2)
