from django.db import models


class NaturepediaModel(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    deleted_on = models.DateTimeField(blank=True, null=True)
    
    class Meta:
        managed = True
        abstract = True
