from django.db import models

from naturepedia.models.living.living_traits.size_entity import SizeEntity
from naturepedia.models.naturepedia_model import NaturepediaModel
from naturepedia.schema.living.animals.animal_input_type import SizeInput


class LivingThing(NaturepediaModel):
    english_name = models.CharField("The name of the living thing", max_length=50, unique=True)
    scientific_name = models.CharField("The scientific name of the living thing", max_length=200)
    size = models.ForeignKey(SizeEntity, on_delete=models.CASCADE)
    consumes = models.CharField("What the living thing consumes - tbd", max_length=100)
    evolved_in = models.PositiveIntegerField("The time the living thing first appeared", help_text="in millions of years ago")

    class Meta: 
        abstract = True

    @classmethod
    def create(
        cls, 
        english_name: str, 
        scientific_name: str, 
        consumes: str, 
        evolved_in: int,
        size_dict: SizeInput
    ):
        livingThing = cls(
            english_name=english_name,
            consumes=consumes,
            evolved_in=evolved_in
        )
        if size_dict: #this whole chunk can be refactored into a method on a custom object manager
            size_dict.set_defaults_on_empty_values() #horrible function
            size_entity, created_flag = SizeEntity.objects.get_or_create(
                    min_height = size_dict.min_height, 
                    max_height = size_dict.max_height,
                    avg_height = size_dict.avg_height, 
                    min_weight = size_dict.min_weight, 
                    max_weight = size_dict.max_weight,
                    avg_weight = size_dict.avg_height
                )
        else:
            size_dict = SizeEntity.create_default().__dict__
            size_entity, created_flag = SizeEntity.objects.get_or_create(
                min_height = size_dict['min_height'], 
                max_height = size_dict['max_height'],
                avg_height = size_dict['avg_height'], 
                min_weight = size_dict['min_weight'], 
                max_weight = size_dict['max_weight'],
                avg_weight = size_dict['avg_height']
            ) # to here get_or_create_default()

        livingThing.size = size_entity
        livingThing.set_or_default_scientific_name(scientific_name)
        return livingThing

    def __str__(self):
        return self.english_name


    def set_or_default_scientific_name(self, scientific_name) -> str:
        if scientific_name is None:
            self.scientific_name = f"{self.english_name}ipusium"
        else:
            self.scientific_name = scientific_name
