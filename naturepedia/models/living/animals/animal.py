from django.db import models

from naturepedia.base.enum.colour_enum import Colour
from naturepedia.models.living.living_thing import LivingThing
from naturepedia.models.living.living_traits.colour_entity import ColourEntity
from naturepedia.schema.living.animals.animal_input_type import ColourInput, SizeInput


class Animal(LivingThing):
    colour = models.ForeignKey(ColourEntity, on_delete=models.PROTECT)

    class Meta:
        abstract = True

    @classmethod
    def create(
        cls, 
        english_name: str, 
        scientific_name: str, 
        consumes: str,
        evolved_in: int,
        size_dict: SizeInput,
        colour: ColourInput = "NONE"
    ):
        animal = super().create(
            english_name, 
            scientific_name,
            consumes, 
            evolved_in,
            size_dict
        )
        animal.colour, _ = ColourEntity.objects.get_or_create(colour=colour.getColourNameFromValue())
        return animal
