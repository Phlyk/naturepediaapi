from django.db import models

from naturepedia.base.enum.movement import Movement
from naturepedia.models.living.animals.animal import Animal


class Bird(Animal):
    movement = Movement.FLY
    wingspan = models.PositiveIntegerField("The length of the birds wings", help_text="in cm")
