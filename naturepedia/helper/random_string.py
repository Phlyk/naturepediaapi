import random
import string


def makeRandomStringLikeAMofugga(base_string: str, string_length: int = 10) -> str:
    halfway = int(string_length / 2)
    return f"{generateRandomCharNbTimes(halfway)}{base_string}{generateRandomCharNbTimes(halfway)}"

def generateRandomCharNbTimes(howManyTimes: int):
    letters_to_chose_from = string.ascii_letters
    randomness = ''
    for i in range(howManyTimes):
        randomness += random.choice(letters_to_chose_from)
    return randomness
