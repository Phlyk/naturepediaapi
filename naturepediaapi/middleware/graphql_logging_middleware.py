import logging

from django.conf import settings

request_logger = logging.getLogger('graphql.request.logger')


class GraphQLLoggingMiddleware(object):
    """
    Provides full logging of requests and responses
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        """
        Adding request and response logging
        """
        if self.is_request_graphql(request):
            request_logger.log(
                logging.INFO,
                f"POST: {request.POST}. body: {request.body}",
                extra={
                    'tags': {
                        'url': request.build_absolute_uri()
                    }
                }
            )
            response = self.get_response(request)
            request_logger.log(
                logging.INFO,
                f"Response code: {response.status_code}. Response content: {response.content}"
            )
            return response
        return self.get_response(request)

    def is_request_graphql(self, request):
        return request.path.startswith('/graphql/') and request.method == "POST" and request.META.get('CONTENT_TYPE') == 'application/json'
