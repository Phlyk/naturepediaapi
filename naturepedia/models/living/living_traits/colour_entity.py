from django.core.exceptions import ValidationError
from django.db import models

from naturepedia.base.enum.colour_enum import Colour
from naturepedia.models.naturepedia_model import NaturepediaModel


class ColourEntity(NaturepediaModel):
    colour = models.CharField(
        max_length=50,
        choices=Colour.choices(),
        default=Colour.NONE.name,
        # validators=[validate_color],
        unique=True
    )

    def save(self, *args, **kwargs):
        '''Each layer should be responsible for its own consistency and the storage
         layer - ie models - shouldn't care about the business layer
         This is an exception because it's enforcing the constraint'''
        self.full_clean()
        super().save(*args, **kwargs)

    def clean(self, *args, **kwargs):
        '''Not needed now that save calls full_clean'''
        if self.colour not in Colour.keys():
            raise ValidationError(
                "%(colour)s is not a valid Colour.",
                code="invalid",
                params={"colour": self.colour}
            )
        super().clean(*args, **kwargs)

    def __str__(self):
        return str(self.colour)

    class Meta:
        db_table = 'naturepedia_colour'

#  Could have a join table that adds Primarily, %, etc
