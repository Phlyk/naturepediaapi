# import graphene
# from graphene_django import DjangoObjectType

# from .models import Vehicle as VehicleModel
# from .models import Car as CarModel
# from .models import Truck as TruckModel

# class Vehicle(graphene.Interface):

#     id = graphene.UUID()
#     brand = graphene.String()

# class Car(graphene.ObjectType):

#     owner = graphene.String()

#     class Meta:

#         interfaces = (Vehicle, )

# class Truck(graphene.ObjectType):

#     company = graphene.String()

#     class Meta:

#         interfaces = (Vehicle, )

# class VehicleUnionType(graphene.Union):

#     @classmethod
#     def resolve_type(cls, instance, info):

#         if isinstance(instance, CarModel):
#             return Car

#         elif isinstance(instance, TruckModel):
#             return Truck

#         return None

#     class Meta:

#         types = [Car, Truck]

# class Query(graphene.ObjectType):

#     vehicles = graphene.List(VehicleUnionType)

#     def resolve_vehicles(self, info, **kwargs):

#         return VehicleModel.objects.all()
