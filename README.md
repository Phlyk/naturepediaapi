# Naturepedia
This is my Naturepedia API as part of my playground project.
It's primary purpose is to let me screw around with Graphene GraphQL Backend and Apollo Client as frontend to support Lingberries development.

## Overview
To the user it will provide an application for a user to CRU(no D) information on Biodiversity around the globe.

### Example Use Cases
- Get all animals that live in the Sahara (Read)
- Find all the things that eat termites (Read)
- Add a newly discovered plant found in the Keralan Jungle (Create)
- A new maxmimum size for a killer whale has been discovered (Update) 

### Domain overview
- Animals live in LocationS and have TraitS
- Plants are found in LocationS and haev TraitS
- LocationS have a Biome (generalised) and those Biomes have Environmental Traits 
- All living things have Size, Weight & Consume to survive

## The future
- Behaviour like patterns (migrations, fauna traversed)
- (better) description of the animals features
