from graphene import ObjectType, Schema

from naturepedia.schema.living.animals.fish_schema import DemFishesArentGoodEnoughForMe, GiveMeThemGoodOlFishesBoy
from naturepedia.schema.living.animals.my_123_schema import GiveMeASquibildyPuff, MakeMeASquibildyPuff


class QueriesOfDoom(
    GiveMeThemGoodOlFishesBoy,
    GiveMeASquibildyPuff
):
    pass
    
class MutationsOfHope(
    DemFishesArentGoodEnoughForMe,
    MakeMeASquibildyPuff
):
    pass

schema = Schema(query=QueriesOfDoom, mutation=MutationsOfHope)
