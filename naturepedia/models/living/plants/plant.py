from django.db import models

from naturepedia.base.enum.colour_enum import Colour
from naturepedia.models.living.living_thing import LivingThing
from naturepedia.models.living.living_traits.colour_entity import ColourEntity


class Plant(LivingThing):
    flower_colour = models.ForeignKey(ColourEntity, on_delete=models.DO_NOTHING, default=Colour.NONE.name)
    flower_smell = models.CharField("The smell of the flower", max_length=50, default="delicious")

    class Meta:
        abstract = True

class Tree(Plant):
    WOOD_TYPE = (
        ("HARD", "Hardwood"),
        ("SOFT", "Softwood")
    )
    wood_type = models.CharField(max_length=10, choices=WOOD_TYPE)

class Grass(Plant):
    spikey = models.BooleanField("Does it go booboo when you touch?")
