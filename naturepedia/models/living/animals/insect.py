from django.db import models

from naturepedia.models.living.animals.animal import Animal


class Insect(Animal):
    flies = models.BooleanField("Can the insect fly")
