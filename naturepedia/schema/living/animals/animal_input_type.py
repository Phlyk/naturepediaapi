import graphene
from django.db.models import Q
from graphene.types import Enum, Field, InputObjectType, Int, String

from naturepedia.base.enum.colour_enum import Colour
from naturepedia.models.living.living_traits.size_entity import SizeEntity


class SizeInput(InputObjectType):
    min_height = Int(required=True)
    max_height = Int(required=True)
    avg_height = Int(required=False)
    min_weight = Int(required=False)
    max_weight = Int(required=False)
    avg_weight = Int(required=False)

    def set_defaults_on_empty_values(self):
        model_ugly_default_size = SizeEntity()
        if self.min_height is None:
            self.min_height = model_ugly_default_size.min_height
        if self.max_height is None:
            self.max_height = model_ugly_default_size.max_height
        if self.avg_height is None:
            self.avg_height = model_ugly_default_size.avg_height
        if self.min_weight is None:
            self.min_weight = model_ugly_default_size.min_weight
        if self.max_weight is None:
            self.max_weight = model_ugly_default_size.max_weight
        if self.avg_weight is None:
            self.avg_weight = model_ugly_default_size.avg_weight


class ColourInputEnum(Enum):
    class Meta:
        enum = Colour

class ColourInput(InputObjectType):
    colour = graphene.InputField(ColourInputEnum, required=False, default_value=Colour.NONE.name)

    def getColourNameFromValue(self):
        return Colour(self.colour).name

class AnimalInput(InputObjectType):
    size_dict = graphene.InputField(SizeInput, name="size")
    english_name = String(required=True)
    scientific_name = String(required=False, description="what do them sciency folks call it?")
    consumes = String(required=True, default_value="the poop of other fish")
    colour = graphene.InputField(ColourInput, default_value=ColourInput())
    evolved_in = Int(required=True)

class DeleteAnimalInput(AnimalInput):
    size = graphene.InputField(SizeInput)
    english_name = String()
    scientific_name = String(description="what do them sciency folks call it?")
    consumes = String()
    colour = graphene.InputField(ColourInput)
    evolved_in = Int()

class InputQFilterBuilder():

    def __init__(self, filter_method):
        super().__init__()
        self.filter_method = filter_method

    def build_filter_from_input(self, input) -> Q:
        """ Q Filter builder method. Assumes the input contains desired `None` keys
            Also assumes that keys of input map directly to columns in DB. Also handles nested objects """
        q_filter = Q() # or Q(pk__in=[]) #logic is wrong but fine for PoC
        for input_item in input.items():
            input_key, input_value = input_item 
            if issubclass(type(input_value), InputObjectType):
                unpacked_key, unpacked_value = input_item
                q_filter.add(self.handle_nested_input_object(unpacked_key, unpacked_value), self.filter_method)
            elif input_value is not None:
                q_filter.add(Q(**{input_key: input_value}), self.filter_method)
        return q_filter

    def handle_nested_input_object(self, key_for_input, nested_input) -> Q:
        """ Should handle nested inputs to infinity using recursion by combining the fields
            in field lookup syntax """
        q_filter = Q()
        for input_key, input_value in nested_input.items():
            if issubclass(type(input_value), InputObjectType):
                return self.handle_nested_input_object(input_key, input_value, self.filter_method)
            q_filter.add(Q(**{f"{key_for_input}__{input_key}": input_value}), self.filter_method)
        return q_filter
