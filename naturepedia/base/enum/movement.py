from enum import Enum


class Movement(Enum):
    FLY = "Flying"
    LAND = "Land movement"
    SWIM = "Swimming"
    NONE = "No movement"
