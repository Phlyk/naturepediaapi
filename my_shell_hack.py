from naturepedia.base.enum.colour_enum import Colour
from naturepedia.models.living.animals.fish import Fish
from naturepedia.models.living.living_traits.colour_entity import ColourEntity
from naturepedia.models.living.living_traits.size_entity import SizeEntity
from naturepedia.models.naturepedia_model import NaturepediaModel
